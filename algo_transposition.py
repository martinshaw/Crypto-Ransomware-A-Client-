# Transposition Cipher
# http://inventwithpython.com/hacking (BSD Licensed)

import math

"""
    Transposition Cipher

    Expects following parameters:
        String plaintext        Message to be processed
        Integer key             Integer. Initial cursor position
        String mode             Either "encrypt" or "decrypt" depending on desired operation
"""

def transposition(message, key, mode):

    if mode.lower() == 'encrypt' or mode.lower() == 'e':

        # Each string in ciphertext represents a column in the grid.
        _output = [''] * key

        # Loop through each column in ciphertext.
        for col in range(key):
            pointer = col

            # Keep looping until pointer goes past the length of the message.
            while pointer < len(message):
                # Place the character at pointer in message at the end of the
                # current column in the ciphertext list.
                _output[col] += message[pointer]

                # move pointer over
                pointer += key

        # Convert the ciphertext list into a single string value and return it.
        return ''.join(_output)

    elif mode.lower() == 'decrypt' or mode.lower() == 'd':

        # The number of "columns" in our transposition grid:
        numOfColumns = math.ceil(len(message) / key)
        # The number of "rows" in our grid will need:
        numOfRows = key
        # The number of "shaded boxes" in the last "column" of the grid:
        numOfShadedBoxes = (numOfColumns * numOfRows) - len(message)

        # Each string in plaintext represents a column in the grid.
        _output = [''] * numOfColumns

        # The col and row variables point to where in the grid the next
        # character in the encrypted message will go.
        col = 0
        row = 0

        for symbol in message:
            _output[col] += symbol
            col += 1  # point to next column

            # If there are no more columns OR we're at a shaded box, go back to
            # the first column and the next row.
            if (col == numOfColumns) or (col == numOfColumns - 1 and row >= numOfRows - numOfShadedBoxes):
                col = 0
                row += 1

        return ''.join(_output)


