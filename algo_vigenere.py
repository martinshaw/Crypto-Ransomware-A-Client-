# Vigenere Polyalphabetic Substitution Cipher
# http://inventwithpython.com/hacking (BSD Licensed)


"""
    Vigenere Polyalphabetic Substitution Cipher

    Expects following parameters:
        String plaintext        Message to be processed
        String key              Short word or short sequence of alphabetical characters (repeated over the
                                length of the message)
        String mode             Either "encrypt" or "decrypt" depending on desired operation
"""

def vigenere(message, key, mode):
    LETTERS = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
    _output = [] # stores the encrypted/decrypted message string

    keyIndex = 0
    key = key.upper()

    for symbol in message: # loop through each character in message
        num = LETTERS.find(symbol.upper())
        if num != -1: # -1 means symbol.upper() was not found in LETTERS
            if mode.lower() == 'encrypt' or mode.lower == "e":
                num += LETTERS.find(key[keyIndex]) # add if encrypting
            elif mode.lower() == 'decrypt' or mode.lower == "d":
                num -= LETTERS.find(key[keyIndex]) # subtract if decrypting

            num %= len(LETTERS) # handle the potential wrap-around

            # add the encrypted/decrypted symbol to the end of translated.
            if symbol.isupper():
                _output.append(LETTERS[num])
            elif symbol.islower():
                _output.append(LETTERS[num].lower())

            keyIndex += 1 # move to the next letter in the key
            if keyIndex == len(key):
                keyIndex = 0
        else:
            # The symbol was not in LETTERS, so add it to translated as is.
            _output.append(symbol)

    return ''.join(_output)
