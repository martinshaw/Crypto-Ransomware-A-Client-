"""
    Import packages
"""
import sys
import random
import base64



"""
    Create reference back to Main module (__init__.py)
"""
main = sys.modules['__main__']



def generate_new_keyset():
    # Generate and add Caesar Key. Note: see algo_caesar.py
    _caesar = random.randint(1, 26)

    # Generate and add Vigenere Key. Note see algo_vigenere.py
    _vigenere = ''.join(random.choice('ABCDEFGHIJKLMNOPQRSTUVWXYZ') for _ in range(10))

    # Generate and add Transposition Key. Note: see algo_transposition.py
    _transposition = random.randint(1, 10)

    # Format key set into Base64 Encoded String for storage and transport
    _combined = str(_caesar) + "//" + str(_vigenere) + "//" + str(_transposition)

    main.l("New keyset generated!")
    return base64.b64encode(_combined.encode('utf-8'))



def access_keyset(key_index, keyset):
    _keyset = base64.b64decode(keyset).decode('utf-8').split("//")

    if key_index == 0:      # Caesar Key        (str => int)
        return int(_keyset[0])
    if key_index == 1:      # Vigenere          (str => str)
        return str(_keyset[1])
    if key_index == 2:      # Transposition     (str => int)
        return int(_keyset[2])