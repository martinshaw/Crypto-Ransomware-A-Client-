# -*- mode: python -*-

block_cipher = None


a = Analysis(['__init__.py'],
             pathex=['C:\\Users\\martin\\Documents\\MMU Year 3\\Network Security\\Crypto Ransomware A'],
             binaries=[],
             datas=[],
             hiddenimports=[],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          name='__init__',
          debug=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True , icon='..\\Crypto Ransomware A Setup and Compilation\\icon.ico')
