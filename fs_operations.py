"""
    Import packages
"""
import sys
import io

"""
    Create reference back to Main module (__init__.py)
"""
main = sys.modules['__main__']


def read_file(file_path):
    with io.open(file_path, 'r') as fileA:
        return fileA.read()


def write_file(file_path, contents):
    with io.open(file_path, 'w') as fileA:
        fileA.write(contents)

    # Read file to check if file was written to and contents are the same
    with io.open(file_path, 'r') as fileB:
        read_contents = fileB.read()

    if contents == read_contents:
        return True
    else:
        return False


def read_bytes_file(file_path):
    with io.open(file_path, 'rb') as fileA:
        return fileA.read()


def write_bytes_file(file_path, contents):
    with io.open(file_path, 'wb') as fileA:
        fileA.write(contents)

    # Read file to check if file was written to and contents are the same
    with io.open(file_path, 'rb') as fileB:
        read_contents = fileB.read()

    if contents == read_contents:
        return True
    else:
        return False


def append_file(file_path, contents):
    with io.open(file_path, 'a') as fileA:
        if fileA.write(contents):
            return True
        else:
            return False
