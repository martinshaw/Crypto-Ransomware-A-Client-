# Caesar Substitution Cipher
# http://inventwithpython.com/hacking (BSD Licensed)


"""
    Caesar Substitution Cipher
    
    Expects following parameters:
        String plaintext        Message to be processed
        Integer key             Integer. Any number higher than 26 won't make a difference to possible outcomes (e.g. 28 same as 2)
        String mode             Either "encrypt" or "decrypt" depending on desired operation
"""

def caesar(message, key, mode):
    # every possible symbol that can be encrypted
    LETTERS = ' !"#$%&\'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~'

    # stores the encrypted/decrypted form of the message
    _output = ''

    # run the encryption/decryption code on each symbol in the message string
    for symbol in message:
        if symbol in LETTERS:
            # get the encrypted (or decrypted) number for this symbol
            num = LETTERS.find(symbol) # get the number of the symbol
            if mode.lower() == 'encrypt' or mode.lower() == 'e':
                num = num + key
            elif mode.lower() == 'decrypt' or mode.lower() == 'd':
                num = num - key

            # handle the wrap-around if num is larger than the length of
            # LETTERS or less than 0
            if num >= len(LETTERS):
                num = num - len(LETTERS)
            elif num < 0:
                num = num + len(LETTERS)

            # add encrypted/decrypted number's symbol at the end of ciphertext
            _output = _output + LETTERS[num]

        else:
            # just add the symbol without encrypting/decrypting
            _output = _output + symbol

    return _output